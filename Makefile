RMD_FILES	:= $(wildcard *.Rmd)
MD_FILES	:= $(patsubst %.Rmd, output/%.md, $(RMD_FILES))
HTML_FILES	:= $(patsubst %.Rmd, output/%.html, $(RMD_FILES))

all: clean html md

md: $(MD_FILES)

html: $(HTML_FILES)

output/%.md: %.Rmd
	@Rscript -e 'rmarkdown::render(input = "$<", output_dir = "output", output_format = "md_document")'

output/%.html: %.Rmd
	@Rscript -e 'rmarkdown::render(input = "$<", output_dir = "output", output_format = "html_document")'

.PHONY: clean

clean:
	@rm -rf output/*
